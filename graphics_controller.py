import matplotlib.pyplot as plt


class GraphicsController:
    @staticmethod  # нарисовать один элементарный участок поля или транспортное средство (представляют собой клетки)
    def draw_square(ax=None, color_map=None, limit_color='black', points=None):
        ax = ax if ax is not None else plt.gca()
        field = plt.Polygon(
            points,
            facecolor=color_map,
            edgecolor=limit_color
        )
        ax.add_patch(field)

    @staticmethod  # нарисовать поле (поле состоит из элементарных участков и транспортных средств на них)
    def draw_field(row_amount, column_amount, cell_size, vehicle_route):
        ax = plt.gca()
        row_number = 0
        while row_number < row_amount * cell_size:
            column_number = 0
            while column_number < column_amount * cell_size:
                if column_number / cell_size == (cell_size - 1) / 2 - 0.5 or column_number / cell_size == (cell_size - 1) / 2 + 0.5 or row_number / cell_size == (cell_size - 1) / 2 - 0.5 or row_number / cell_size == (cell_size - 1) / 2 + 0.5:
                    GraphicsController.draw_square(
                        ax=ax,
                        color_map=(0.5, 0.5, 0.5, 0.5),  # проезжая часть - серый цвет
                        points=[
                            (column_number, row_number),
                            (column_number + cell_size, row_number),
                            (column_number + cell_size, row_number + cell_size),
                            (column_number, row_number + cell_size)
                        ]
                    )
                else:
                    GraphicsController.draw_square(
                        ax=ax,
                        color_map=(0.0, 0.0, 0.0, 0.0),  # участки, не относящиеся к проезжей части - белый цвет
                        points=[
                            (column_number, row_number),
                            (column_number + cell_size, row_number),
                            (column_number + cell_size, row_number + cell_size),
                            (column_number, row_number + cell_size)
                        ]
                    )
                column_number += cell_size
            row_number += cell_size
        for elementary_area in vehicle_route:
            GraphicsController.draw_square(
                color_map=(0.0, 0.0, 0.0, 1.0),  # транспортные средства на проезжей части - черный цвет
                points=[
                    (column_amount * (elementary_area[0] % column_amount) + 3, row_amount * (elementary_area[0] // row_amount) + 3),
                    (column_amount * (elementary_area[0] % column_amount) + 6, row_amount * (elementary_area[0] // row_amount) + 3),
                    (column_amount * (elementary_area[0] % column_amount) + 6, row_amount * (elementary_area[0] // row_amount) + 6),
                    (column_amount * (elementary_area[0] % column_amount) + 3, row_amount * (elementary_area[0] // row_amount) + 6)
                ]
            )
        plt.xlim(0, column_amount * cell_size)
        plt.ylim(row_amount * cell_size, 0)
        ax.set_aspect("equal")
        plt.show()
