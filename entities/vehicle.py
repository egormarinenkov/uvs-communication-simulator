from random import choice


class Vehicle:
    def __init__(self, initial_speed, field, existing_vehicle_routes, is_saboteur):
        self.initial_speed = initial_speed
        self.field = field
        self.existing_vehicle_routes = existing_vehicle_routes
        # self.truth = {}
        self.reputation = dict()
        self.sum_action = dict()
        self.is_saboteur = is_saboteur
        self.steps = 1
        self.is_blocked = dict()
        self.old_time = dict()
        self.reputation_time = dict()

    def add_vehicle(self, vehicle, iteration):
        if vehicle not in self.reputation.keys():
            self.reputation[vehicle] = 0.5
            self.sum_action[vehicle] = 0.5
            self.is_blocked[vehicle] = False
            self.old_time[vehicle] = iteration
            self.reputation_time[vehicle] = 1

    # def del_vehicle(self):

    def choose_movement_direction(self):  # рандомно выбрать направление движения (прямо, налево или направо)
        possible_movement_variants = ["go_directly", "turn_left", "turn_right"]
        return choice(possible_movement_variants)

    def determine_start_point(self):
        # элементарные участки, находящиеся на краях заданного поля
        possible_start_points = [min(road.get_road_cells()) for road in self.field.get_roads()
                                 if road.get_road_direction() == "codirectional"]
        possible_start_points.extend([max(road.get_road_cells()) for road in self.field.get_roads()
                                      if road.get_road_direction() == "contradirectional"])
        # элементарные участки, которые при вхождении новым автомобилем будут заняты другими ТС
        already_occupied_start_points = []
        for existing_vehicle_route in self.existing_vehicle_routes:
            already_occupied_start_points.append(existing_vehicle_route[0])
        free_start_points = []
        for possible_start_point in possible_start_points:
            if possible_start_point not in already_occupied_start_points:
                free_start_points.append(possible_start_point)
        if len(free_start_points) == 0:
            start_point = -1
        else:
            start_point = choice(free_start_points)
        return start_point

    def point_is_on_horizontal_road(self, point):
        for road in self.field.get_roads():
            if point in road.get_road_cells() and road.get_road_type() == "horizontal":
                return True
        return False

    def point_is_on_vertical_road(self, point):
        for road in self.field.get_roads():
            if point in road.get_cells() and road.get_road_type() == "vertical":
                return True
        return False

    def determine_vehicle_route(self):
        start_point = self.determine_start_point()
        if start_point == -1:  # если все возможные стартовые точки заняты, машина не входит в поле
            vehicle_route = []
        else:
            vehicle_route = []
            related_elementary_areas_on_intersections = []
            step_length = self.initial_speed
            movement_direction = self.choose_movement_direction()
            if movement_direction == "go_directly":
                if self.point_is_on_horizontal_road(start_point):
                    for elementary_area_on_intersection in self.field.get_elementary_areas_on_intersections():
                        if elementary_area_on_intersection // self.field.get_column_amount() == start_point // self.field.get_column_amount():
                            related_elementary_areas_on_intersections.append(elementary_area_on_intersection)
                    if start_point < min(related_elementary_areas_on_intersections):
                        right_limit = start_point + self.field.get_column_amount()
                        while start_point < right_limit:
                            vehicle_route.append(start_point)
                            if start_point == min(related_elementary_areas_on_intersections):  # снижение скорости на перекрестке до 1
                                start_point += 1
                            else:
                                start_point += step_length
                    else:
                        left_limit = start_point - self.field.get_column_amount()
                        while start_point > left_limit:
                            vehicle_route.append(start_point)
                            if start_point == max(related_elementary_areas_on_intersections):
                                start_point -= 1
                            else:
                                start_point -= step_length
                else:
                    for elementary_area_on_intersection in self.field.get_elementary_areas_on_intersections():
                        if elementary_area_on_intersection % self.field.get_row_amount() == start_point % self.field.get_row_amount():
                            related_elementary_areas_on_intersections.append(elementary_area_on_intersection)
                    if start_point < min(related_elementary_areas_on_intersections):
                        bottom_limit = start_point + self.field.get_row_amount() * self.field.get_column_amount()
                        while start_point < bottom_limit:
                            vehicle_route.append(start_point)
                            if start_point == min(related_elementary_areas_on_intersections):
                                start_point += self.field.get_column_amount()
                            else:
                                start_point += step_length * self.field.get_column_amount()
                    else:
                        up_limit = start_point - self.field.get_row_amount() * self.field.get_column_amount()
                        while start_point > up_limit:
                            vehicle_route.append(start_point)
                            if start_point == max(related_elementary_areas_on_intersections):
                                start_point -= self.field.get_column_amount()
                            else:
                                start_point -= step_length * self.field.get_column_amount()
            elif movement_direction == "turn_left":
                if self.point_is_on_horizontal_road(start_point):
                    for elementary_area_on_intersection in self.field.get_elementary_areas_on_intersections():
                        if elementary_area_on_intersection // self.field.get_column_amount() == start_point // self.field.get_column_amount():
                            related_elementary_areas_on_intersections.append(elementary_area_on_intersection)
                    if start_point < min(related_elementary_areas_on_intersections):
                        while start_point < max(related_elementary_areas_on_intersections):
                            vehicle_route.append(start_point)
                            if start_point == min(related_elementary_areas_on_intersections):
                                start_point += 1
                            else:
                                start_point += step_length
                        up_limit = start_point - self.field.get_row_amount() * self.field.get_column_amount() / 2
                        while start_point >= up_limit:
                            vehicle_route.append(start_point)
                            if start_point == max(related_elementary_areas_on_intersections):
                                start_point -= self.field.get_column_amount()
                            else:
                                start_point -= step_length * self.field.get_column_amount()
                    else:
                        while start_point > min(related_elementary_areas_on_intersections):
                            vehicle_route.append(start_point)
                            if start_point == max(related_elementary_areas_on_intersections):
                                start_point -= 1
                            else:
                                start_point -= step_length
                        bottom_limit = start_point + self.field.get_row_amount() * self.field.get_column_amount() / 2
                        while start_point <= bottom_limit:
                            vehicle_route.append(start_point)
                            if start_point == min(related_elementary_areas_on_intersections):
                                start_point += self.field.get_column_amount()
                            else:
                                start_point += step_length * self.field.get_column_amount()
                else:
                    for elementary_area_on_intersection in self.field.get_elementary_areas_on_intersections():
                        if elementary_area_on_intersection % self.field.get_column_amount() == start_point % self.field.get_column_amount():
                            related_elementary_areas_on_intersections.append(elementary_area_on_intersection)
                    if start_point < min(related_elementary_areas_on_intersections):
                        while start_point < max(related_elementary_areas_on_intersections):
                            vehicle_route.append(start_point)
                            if start_point == min(related_elementary_areas_on_intersections):
                                start_point += self.field.get_column_amount()
                            else:
                                start_point += step_length * self.field.get_column_amount()
                        right_limit = start_point + self.field.get_column_amount() / 2
                        while start_point <= right_limit:
                            vehicle_route.append(start_point)
                            if start_point == max(related_elementary_areas_on_intersections):
                                start_point += 1
                            else:
                                start_point += step_length
                    else:
                        while start_point > min(related_elementary_areas_on_intersections):
                            vehicle_route.append(start_point)
                            if start_point == max(related_elementary_areas_on_intersections):
                                start_point -= self.field.get_column_amount()
                            else:
                                start_point -= step_length * self.field.get_column_amount()
                        left_limit = start_point - self.field.get_column_amount() / 2
                        while start_point >= left_limit:
                            vehicle_route.append(start_point)
                            if start_point == min(related_elementary_areas_on_intersections):
                                start_point -= 1
                            else:
                                start_point -= step_length
            else:
                if self.point_is_on_horizontal_road(start_point):
                    for elementary_area_on_intersection in self.field.get_elementary_areas_on_intersections():
                        if elementary_area_on_intersection // self.field.get_column_amount() == start_point // self.field.get_column_amount():
                            related_elementary_areas_on_intersections.append(elementary_area_on_intersection)
                    if start_point < min(related_elementary_areas_on_intersections):
                        while start_point < min(related_elementary_areas_on_intersections):
                            vehicle_route.append(start_point)
                            start_point += step_length
                        bottom_limit = start_point + self.field.get_row_amount() * self.field.get_column_amount() / 2
                        while start_point < bottom_limit:
                            vehicle_route.append(start_point)
                            start_point += step_length * self.field.get_column_amount()
                    else:
                        while start_point > max(related_elementary_areas_on_intersections):
                            vehicle_route.append(start_point)
                            start_point -= step_length
                        up_limit = start_point - self.field.get_row_amount() * self.field.get_column_amount() / 2
                        while start_point > up_limit:
                            vehicle_route.append(start_point)
                            start_point -= step_length * self.field.get_column_amount()
                else:
                    for elementary_area_on_intersection in self.field.get_elementary_areas_on_intersections():
                        if elementary_area_on_intersection % self.field.get_column_amount() == start_point % self.field.get_column_amount():
                            related_elementary_areas_on_intersections.append(elementary_area_on_intersection)
                    if start_point < min(related_elementary_areas_on_intersections):
                        while start_point < min(related_elementary_areas_on_intersections):
                            vehicle_route.append(start_point)
                            start_point += step_length * self.field.get_column_amount()
                        left_limit = start_point - self.field.get_column_amount() / 2
                        while start_point > left_limit:
                            vehicle_route.append(start_point)
                            start_point -= step_length
                    else:
                        while start_point > max(related_elementary_areas_on_intersections):
                            vehicle_route.append(start_point)
                            start_point -= step_length * self.field.get_column_amount()
                        right_limit = start_point + self.field.get_column_amount() / 2
                        while start_point < right_limit:
                            vehicle_route.append(start_point)
                            start_point += step_length
        return vehicle_route
