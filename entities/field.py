class Field:
    def __init__(self, row_amount, column_amount, roads):
        self.row_amount = row_amount
        self.column_amount = column_amount
        self.roads = roads

    def get_row_amount(self):
        return self.row_amount

    def get_column_amount(self):
        return self.column_amount

    def get_roads(self):
        return self.roads

    def get_elementary_areas_on_intersections(self):
        vertical_roads, horizontal_roads, intersections = [], [], []
        for road in self.roads:
            if road.get_road_type() == "vertical":
                vertical_roads.append(road)
            else:
                horizontal_roads.append(road)
        for vertical_road in vertical_roads:
            for elementary_area_on_vertical_road in vertical_road.get_road_cells():
                for horizontal_road in horizontal_roads:
                    for elementary_area_on_horizontal_road in horizontal_road.get_road_cells():
                        if elementary_area_on_horizontal_road == elementary_area_on_vertical_road:
                            intersections.append(elementary_area_on_vertical_road)
        return intersections
