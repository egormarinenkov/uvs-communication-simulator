class Road:
    def __init__(self, road_type, road_direction, road_cells):
        self.road_type = road_type
        self.road_direction = road_direction
        self.road_cells = road_cells

    def get_road_direction(self):
        return self.road_direction

    def get_road_type(self):
        return self.road_type

    def get_road_cells(self):
        return self.road_cells
