from entities.road import *
from entities.field import *
from entities.vehicle import *
# from graphics_controller import *
# from random import randint
from math import exp, pow, log, sqrt
import numpy as np
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

row_amount = 10  # количество рядов
column_amount = 10  # количество столбцов
cell_size = 10  # длина/ширина одного элементарного участка
# vehicle_appearance_probability = 1  # вероятность появления нового ТС на дороге за дискрету времени

# EXPERIMENT SETUP

data_type_amount = 1
data_type_coefficients = [{"a": 0.5, "a'": 0.2}]    # 0.5 0.2
saboteur_percentage = 0.3

experiment_amount = 1000  # 1000
iterations_in_experiment = 1000  # 1000
total_UV = 1000    # 1000
sensors_radius = 1
communication_radius = 10

experiment_type = "Classic reputation"
# experiment_type = "Game theory"

path_to_file = experiment_type + "_" + str(saboteur_percentage) + ".txt"

roads = [  # карта дорог
    Road(
        road_type="vertical",
        road_direction="codirectional",
        road_cells=[4, 14, 24, 34, 44, 54, 64, 74, 84, 94]
    ),
    Road(
        road_type="vertical",
        road_direction="contradirectional",
        road_cells=[95, 85, 75, 65, 55, 45, 35, 25, 15, 5]
    ),
    Road(
        road_type="horizontal",
        road_direction="contradirectional",
        road_cells=[49, 48, 47, 46, 45, 44, 43, 42, 41, 40]
    ),
    Road(
        road_type="horizontal",
        road_direction="codirectional",
        road_cells=[50, 51, 52, 53, 54, 55, 56, 57, 58, 59]
    )
]

# поле заданной длины и ширины с картой дорог
field = Field(row_amount=row_amount, column_amount=column_amount, roads=roads)


def check_if_conflicts_are_presented(vehicle_routes, iteration):
    conflicts_are_presented = False
    conflicts_info, unique_conflicting_pairs = [], []
    for i in range(len(vehicle_routes)):
        for j in range(len(vehicle_routes)):
            if i != j:
                min_vehicle_route_length = min(len(vehicle_routes[i]), len(vehicle_routes[j]))
                for elementary_area_in_route in range(min_vehicle_route_length):
                    if vehicle_routes[i][elementary_area_in_route] == vehicle_routes[j][elementary_area_in_route]:
                        if i not in unique_conflicting_pairs and j not in unique_conflicting_pairs:
                            unique_conflicting_pairs.append(i)
                            unique_conflicting_pairs.append(j)
                            conflicts_info.append(
                                {
                                    "elementary_area": vehicle_routes[i][elementary_area_in_route],
                                    "index_in_vehicle_route": elementary_area_in_route
                                }
                            )
                            conflicts_are_presented = True
    return conflicts_are_presented, conflicts_info


def calculate_intersection_bandwidth(vehicle_routes, route_in_conflict, index_in_vehicle_route,
                                     route_index_in_conflict):
    elementary_areas_on_intersection = field.get_elementary_areas_on_intersections()
    if route_in_conflict[index_in_vehicle_route] in elementary_areas_on_intersection and \
            route_in_conflict[index_in_vehicle_route - 1] not in elementary_areas_on_intersection:
        if route_in_conflict[index_in_vehicle_route] - route_in_conflict[index_in_vehicle_route - 1] == 2:
            route_in_conflict.insert(index_in_vehicle_route, route_in_conflict[index_in_vehicle_route - 1] + 1)
        elif route_in_conflict[index_in_vehicle_route] - route_in_conflict[index_in_vehicle_route - 1] == 1:
            route_in_conflict.insert(index_in_vehicle_route, route_in_conflict[index_in_vehicle_route - 1])
        elif route_in_conflict[index_in_vehicle_route - 1] - route_in_conflict[index_in_vehicle_route] == 2:
            route_in_conflict.insert(index_in_vehicle_route, route_in_conflict[index_in_vehicle_route - 1] - 1)
        elif route_in_conflict[index_in_vehicle_route - 1] - route_in_conflict[index_in_vehicle_route] == 1:
            route_in_conflict.insert(index_in_vehicle_route, route_in_conflict[index_in_vehicle_route - 1])
        elif route_in_conflict[index_in_vehicle_route] - route_in_conflict[index_in_vehicle_route - 1] == 20:
            route_in_conflict.insert(index_in_vehicle_route, route_in_conflict[index_in_vehicle_route - 1] + 10)
        elif route_in_conflict[index_in_vehicle_route] - route_in_conflict[index_in_vehicle_route - 1] == 10:
            route_in_conflict.insert(index_in_vehicle_route, route_in_conflict[index_in_vehicle_route - 1])
        elif route_in_conflict[index_in_vehicle_route - 1] - route_in_conflict[index_in_vehicle_route] == 20:
            route_in_conflict.insert(index_in_vehicle_route, route_in_conflict[index_in_vehicle_route - 1] - 10)
        elif route_in_conflict[index_in_vehicle_route - 1] - route_in_conflict[index_in_vehicle_route] == 10:
            route_in_conflict.insert(index_in_vehicle_route, route_in_conflict[index_in_vehicle_route - 1])
    elif route_in_conflict[index_in_vehicle_route] in elementary_areas_on_intersection and \
            route_in_conflict[index_in_vehicle_route - 1] in elementary_areas_on_intersection:
        route_in_conflict.insert(index_in_vehicle_route, route_in_conflict[index_in_vehicle_route - 1])
    vehicle_routes[route_index_in_conflict] = route_in_conflict
    max_crossing_intersection_time = 0
    crossing_intersection_steps = 0
    for vehicle_route in vehicle_routes:
        crossing_intersection_time = 0
        for elementary_area in vehicle_route:
            if elementary_area in elementary_areas_on_intersection:
                crossing_intersection_time += 1
        if crossing_intersection_time > max_crossing_intersection_time:
            max_crossing_intersection_time = crossing_intersection_time
        for elementary_area in list(set(vehicle_route)):
            if elementary_area in elementary_areas_on_intersection:
                crossing_intersection_steps += 1
    del route_in_conflict[index_in_vehicle_route]
    return crossing_intersection_steps / max_crossing_intersection_time


def resolve_vehicle_conflicts(vehicles, vehicle_routes, conflict_info):
    is_saboteur = False
    routes_in_conflict, routes_indexes_in_conflict = [], []
    for i in range(len(vehicle_routes)):
        if len(vehicle_routes[i]) >= conflict_info["index_in_vehicle_route"] + 1:
            if vehicle_routes[i][conflict_info["index_in_vehicle_route"]] == \
                    conflict_info["elementary_area"]:
                routes_in_conflict.append(vehicle_routes[i])
                routes_indexes_in_conflict.append(i)
    need_to_reduce_route = False
    difference = 0
    car_already_stays = False
    for i in range(len(routes_in_conflict)):
        if routes_in_conflict[i].count(routes_in_conflict[i][conflict_info["index_in_vehicle_route"]]) > 1:
            car_already_stays = True
            if routes_in_conflict[i][conflict_info["index_in_vehicle_route"]] == routes_in_conflict[i][
                conflict_info["index_in_vehicle_route"] - 1]:
                need_to_reduce_route = True
                index_to_go = i
    elementary_areas_on_intersection = field.get_elementary_areas_on_intersections()
    if need_to_reduce_route:
        possible_vehicle_routes = [[50, 51, 52, 53, 54, 55, 56, 57, 58, 59],
                                   [50, 51, 52, 53, 54, 55, 45, 35, 25, 15, 5], [50, 51, 52, 53, 54, 64, 74, 84, 94],
                                   [49, 48, 47, 46, 45, 44, 43, 42, 41, 40],
                                   [49, 48, 47, 46, 45, 44, 54, 64, 74, 84, 94], [49, 48, 47, 46, 45, 35, 25, 15, 5],
                                   [95, 85, 75, 65, 55, 45, 35, 25, 15, 5], [95, 85, 75, 65, 55, 56, 57, 58, 59],
                                   [95, 85, 75, 65, 55, 45, 44, 43, 42, 41, 40],
                                   [4, 14, 24, 34, 44, 54, 64, 74, 84, 94], [4, 14, 24, 34, 44, 43, 42, 41, 40],
                                   [4, 14, 24, 34, 44, 54, 55, 56, 57, 58, 59]]
        route_to_be_deleted = list(set(vehicle_routes[routes_indexes_in_conflict[index_to_go]]))
        max_conincidences = -1
        for possible_vehicle_route in possible_vehicle_routes:
            count = 0
            for elementary_area1 in possible_vehicle_route:
                for elementary_area2 in route_to_be_deleted:
                    if elementary_area1 == elementary_area2:
                        count += 1
                        if count > max_conincidences:
                            max_conincidences = count
                            needed_possible_vehicle_route = possible_vehicle_route
        difference = len(needed_possible_vehicle_route) - len(route_to_be_deleted)

        if vehicles[routes_indexes_in_conflict[index_to_go]].is_saboteur:
            is_saboteur = True
        del vehicle_routes[routes_indexes_in_conflict[index_to_go]]  # если с данной машиной нет разъезда, удалить ее
        del vehicles[routes_indexes_in_conflict[index_to_go]]
    else:
        if car_already_stays:
            max_stay = -1
            for i in range(len(routes_in_conflict)):
                if routes_in_conflict[i].count(
                        routes_in_conflict[i][conflict_info["index_in_vehicle_route"]]) > max_stay:
                    max_stay = routes_in_conflict[i].count(
                        routes_in_conflict[i][conflict_info["index_in_vehicle_route"]])
                    index_to_go = i
        else:
            bandwidths = []
            for i in range(len(routes_in_conflict)):
                bandwidths.append(calculate_intersection_bandwidth(vehicle_routes, routes_in_conflict[i],
                                                                   conflict_info["index_in_vehicle_route"],
                                                                   routes_indexes_in_conflict[i]))
            index_to_go = bandwidths.index(min(bandwidths))
        for i in range(len(routes_indexes_in_conflict)):
            if i != index_to_go:
                if vehicle_routes[routes_indexes_in_conflict[i]][
                    conflict_info["index_in_vehicle_route"]] in elementary_areas_on_intersection and \
                        vehicle_routes[routes_indexes_in_conflict[i]][
                            conflict_info["index_in_vehicle_route"] - 1] not in elementary_areas_on_intersection:
                    if vehicle_routes[routes_indexes_in_conflict[i]][conflict_info["index_in_vehicle_route"]] - \
                            vehicle_routes[routes_indexes_in_conflict[i]][
                                conflict_info["index_in_vehicle_route"] - 1] == 2:
                        vehicle_routes[routes_indexes_in_conflict[i]].insert(conflict_info["index_in_vehicle_route"],
                                                                             vehicle_routes[
                                                                                 routes_indexes_in_conflict[i]][
                                                                                 conflict_info[
                                                                                     "index_in_vehicle_route"] - 1] + 1)
                    elif vehicle_routes[routes_indexes_in_conflict[i]][conflict_info["index_in_vehicle_route"]] - \
                            vehicle_routes[routes_indexes_in_conflict[i]][
                                conflict_info["index_in_vehicle_route"] - 1] == 1:
                        vehicle_routes[routes_indexes_in_conflict[i]].insert(conflict_info["index_in_vehicle_route"],
                                                                             vehicle_routes[
                                                                                 routes_indexes_in_conflict[i]][
                                                                                 conflict_info[
                                                                                     "index_in_vehicle_route"] - 1])
                    elif vehicle_routes[routes_indexes_in_conflict[i]][conflict_info["index_in_vehicle_route"] - 1] - \
                            vehicle_routes[routes_indexes_in_conflict[i]][conflict_info["index_in_vehicle_route"]] == 2:
                        vehicle_routes[routes_indexes_in_conflict[i]].insert(conflict_info["index_in_vehicle_route"],
                                                                             vehicle_routes[
                                                                                 routes_indexes_in_conflict[i]][
                                                                                 conflict_info[
                                                                                     "index_in_vehicle_route"] - 1] - 1)
                    elif vehicle_routes[routes_indexes_in_conflict[i]][conflict_info["index_in_vehicle_route"] - 1] - \
                            vehicle_routes[routes_indexes_in_conflict[i]][conflict_info["index_in_vehicle_route"]] == 1:
                        vehicle_routes[routes_indexes_in_conflict[i]].insert(conflict_info["index_in_vehicle_route"],
                                                                             vehicle_routes[
                                                                                 routes_indexes_in_conflict[i]][
                                                                                 conflict_info[
                                                                                     "index_in_vehicle_route"] - 1])
                    elif vehicle_routes[routes_indexes_in_conflict[i]][conflict_info["index_in_vehicle_route"]] - \
                            vehicle_routes[routes_indexes_in_conflict[i]][
                                conflict_info["index_in_vehicle_route"] - 1] == 20:
                        vehicle_routes[routes_indexes_in_conflict[i]].insert(conflict_info["index_in_vehicle_route"],
                                                                             vehicle_routes[
                                                                                 routes_indexes_in_conflict[i]][
                                                                                 conflict_info[
                                                                                     "index_in_vehicle_route"] - 1] + 10)
                    elif vehicle_routes[routes_indexes_in_conflict[i]][conflict_info["index_in_vehicle_route"]] - \
                            vehicle_routes[routes_indexes_in_conflict[i]][
                                conflict_info["index_in_vehicle_route"] - 1] == 10:
                        vehicle_routes[routes_indexes_in_conflict[i]].insert(conflict_info["index_in_vehicle_route"],
                                                                             vehicle_routes[
                                                                                 routes_indexes_in_conflict[i]][
                                                                                 conflict_info[
                                                                                     "index_in_vehicle_route"] - 1])
                    elif vehicle_routes[routes_indexes_in_conflict[i]][conflict_info["index_in_vehicle_route"] - 1] - \
                            vehicle_routes[routes_indexes_in_conflict[i]][
                                conflict_info["index_in_vehicle_route"]] == 20:
                        vehicle_routes[routes_indexes_in_conflict[i]].insert(conflict_info["index_in_vehicle_route"],
                                                                             vehicle_routes[
                                                                                 routes_indexes_in_conflict[i]][
                                                                                 conflict_info[
                                                                                     "index_in_vehicle_route"] - 1] - 10)
                    elif vehicle_routes[routes_indexes_in_conflict[i]][conflict_info["index_in_vehicle_route"] - 1] - \
                            vehicle_routes[routes_indexes_in_conflict[i]][
                                conflict_info["index_in_vehicle_route"]] == 10:
                        vehicle_routes[routes_indexes_in_conflict[i]].insert(conflict_info["index_in_vehicle_route"],
                                                                             vehicle_routes[
                                                                                 routes_indexes_in_conflict[i]][
                                                                                 conflict_info[
                                                                                     "index_in_vehicle_route"] - 1])
                elif vehicle_routes[routes_indexes_in_conflict[i]][
                    conflict_info["index_in_vehicle_route"]] in elementary_areas_on_intersection and \
                        vehicle_routes[routes_indexes_in_conflict[i]][
                            conflict_info["index_in_vehicle_route"] - 1] in elementary_areas_on_intersection:
                    vehicle_routes[routes_indexes_in_conflict[i]].insert(conflict_info["index_in_vehicle_route"],
                                                                         vehicle_routes[routes_indexes_in_conflict[i]][
                                                                             conflict_info[
                                                                                 "index_in_vehicle_route"] - 1])
                elif vehicle_routes[routes_indexes_in_conflict[i]][
                    conflict_info["index_in_vehicle_route"]] not in elementary_areas_on_intersection and \
                        vehicle_routes[routes_indexes_in_conflict[i]][
                            conflict_info["index_in_vehicle_route"] - 1] not in elementary_areas_on_intersection:
                    vehicle_routes[routes_indexes_in_conflict[i]].insert(conflict_info["index_in_vehicle_route"],
                                                                         vehicle_routes[routes_indexes_in_conflict[i]][
                                                                             conflict_info[
                                                                                 "index_in_vehicle_route"] - 1])
    return vehicle_routes, need_to_reduce_route, difference, is_saboteur


def calculate_reputation(vehicle_subj, vehicle_obj, truth):
    vehicle_subj.reputation_time[vehicle_obj] += 1
    if truth >= 0.5:
        sum_action = vehicle_subj.sum_action[vehicle_obj] + truth
        reputation = sum_action / (vehicle_subj.reputation_time[vehicle_obj])
        vehicle_subj.sum_action[vehicle_obj] = sum_action
        vehicle_subj.reputation[vehicle_obj] = reputation
        return reputation
    else:
        sum_action = vehicle_subj.sum_action[vehicle_obj] + truth - \
                     (vehicle_subj.reputation[vehicle_obj] - exp(-(1 - truth * (vehicle_subj.reputation_time[vehicle_obj]))))
        reputation = sum_action / (vehicle_subj.reputation_time[vehicle_obj])
        vehicle_subj.sum_action[vehicle_obj] = sum_action
        vehicle_subj.reputation[vehicle_obj] = reputation
        return reputation


def ask_other(vehicle_routes, vehicles, index_obj, index_subj):
    n_truth = 0
    # truth = 0
    for vehicle_route in vehicle_routes:
        if vehicle_routes.index(vehicle_route) != index_obj and vehicle_routes.index(vehicle_route) != index_subj:
            if not vehicles[vehicle_routes.index(vehicle_route)].is_blocked[vehicles[index_obj]]:
                if is_visible(vehicle_routes, index_obj, vehicle_routes.index(vehicle_route)):
                    n_truth += 1
    return n_truth


def is_visible(vehicle_routes, index_obj, index_subj):
    vehicle_route_obj = vehicle_routes[index_obj]
    vehicle_route_subj = vehicle_routes[index_subj]
    if abs(vehicle_route_obj[0] // 10 - vehicle_route_subj[0] // 10) <= sensors_radius \
            and abs(vehicle_route_obj[0] % 10 - vehicle_route_subj[0] % 10) <= sensors_radius:
        return True
    return False


def communication_is_possible(vehicle_route_obj, vehicle_route_subj):
    if abs(vehicle_route_obj[0] // 10 - vehicle_route_subj[0] // 10) <= communication_radius \
            and abs(vehicle_route_obj[0] % 10 - vehicle_route_subj[0] % 10) <= communication_radius:
        return True
    return False


def check_performance(vehicles, vehicle_routes, iteration, total_not_full_data_situation, truth_total):
    # iteration_old = iteration - 1

    for i in range(len(vehicles)):
        for j in range(len(vehicles)):
            if i != j and not vehicles[i].is_blocked[vehicles[j]] and \
                    communication_is_possible(vehicle_routes[j], vehicle_routes[i]):
                truth = 0
                for data_type_index in range(data_type_amount):
                    iteration_old = vehicles[i].old_time[vehicles[j]]
                    # action
                    if vehicles[j].is_saboteur:
                        if iteration % 3 == 0 or iteration % 3 == 1:
                            action = 1
                        else:
                            action = 0
                    else:
                        action = np.random.choice(np.arange(0, 2), p=[0.9, 0.1])
                        # action = 0
                    # truth
                    if action == 1:
                        if is_visible(vehicle_routes, j, i):
                            truth = 0
                        elif ask_other(vehicle_routes, vehicles, j, i) > 0:
                            truth = 0
                        else:
                            if experiment_type == "Classic reputation":
                                truth = 0.5
                            else:
                                value = (1 - pow(data_type_coefficients[data_type_index]["a"],
                                                 iteration_old - iteration)
                                         * (-log(data_type_coefficients[data_type_index]["a"])
                                            * (iteration - (iteration_old - 1))
                                            + 1)) / (1 - pow(data_type_coefficients[data_type_index]["a'"], -1)
                                                     * (-log(data_type_coefficients[data_type_index]["a'"]) + 1))

                                if value <= 1:
                                    truth = value
                                else:
                                    truth = 1

                            truth_total += truth
                            total_not_full_data_situation += 1

                    else:
                        if is_visible(vehicle_routes, j, i):
                            truth = 1
                        elif ask_other(vehicle_routes, vehicles, j, i) > 0:
                            truth = 1
                        else:
                            if experiment_type == "Classic reputation":
                                truth = 0.5
                            else:
                                value = (1 - pow(data_type_coefficients[data_type_index]["a"], iteration_old - iteration)
                                         * (-log(data_type_coefficients[data_type_index]["a"])
                                            * (iteration - (iteration_old - 1))
                                            + 1)) / (1 - pow(data_type_coefficients[data_type_index]["a'"], -1)
                                                     * (-log(data_type_coefficients[data_type_index]["a'"]) + 1))
                                if value <= 1:
                                    truth = value
                                else:
                                    truth = 1

                            truth_total += truth
                            total_not_full_data_situation += 1
                R = vehicles[i].reputation[vehicles[j]]
                calculate_reputation(vehicles[i], vehicles[j], truth)
                trust = sqrt(pow(R, 2) + pow(truth, 2)) - sqrt(pow(1 - R, 2) + pow(1 - truth, 2))

                if trust < 0:
                    vehicles[i].is_blocked[vehicles[j]] = True

                vehicles[i].old_time[vehicles[j]] = iteration
    return total_not_full_data_situation, truth_total


def start_moving(experiment_number):
    total_generated = 0
    total_finished = 0
    total_vehicle_amount = 0  # Сколько всего машин прошло перекресток за заданное число шагов
    total_saboteur_amount = 0
    total_step_amount = 0  # Сколько всего шагов было сделано машинами, чтобы пройти перекресток
    vehicle_routes = []
    vehicles = []
    tp, fp, tn, fn, detected_saboteurs = 0, 0, 0, 0, 0

    iteration = 0
    total_not_full_data_situation = 0
    truth_total = 0

    logger.info("Mode: %s Percentages %f Experiment %d", experiment_type, saboteur_percentage, experiment_number)

    # for iteration in range(iterations_in_experiment):  # Начать взаимодействие на перекрестке
    while total_finished < total_UV:
        iteration += 1
        # logger.info("Experiment %d iteration %d generated (total: %d saboteurs: %d) finished %d now %d",
                    # experiment_number, iteration, total_generated, total_saboteur_amount, total_finished, len(vehicles))
        # GraphicsController.draw_field(row_amount, column_amount, cell_size, vehicle_routes)
        if iteration != 0:
            total_not_full_data_situation,  truth_total = check_performance(vehicles, vehicle_routes, iteration, total_not_full_data_situation, truth_total)
            for j in range(len(vehicle_routes)):  # удаление пройденных машинами элементарных участков
                del vehicle_routes[j][0]
                total_step_amount += 1
                vehicles[j].steps += 1
            copy_vehicle_routes = vehicle_routes.copy()
            copy_vehicles = vehicles.copy()
            '''
            for i in range(len(copy_vehicle_routes)):
                if not copy_vehicle_routes[i]:
                    for j in range(len(copy_vehicle_routes)):
                        if i != j:
                            if not vehicles[j].is_blocked[vehicles[i]] and not vehicles[i].is_saboteur:
                                tn += 1
                            if not vehicles[j].is_blocked[vehicles[i]] and vehicles[i].is_saboteur:
                                fn += 1
                            if vehicles[j].is_blocked[vehicles[i]] and vehicles[i].is_saboteur:
                                tp += 1
                            if vehicles[j].is_blocked[vehicles[i]] and not vehicles[i].is_saboteur:
                                fp += 1
                    total_finished += 1
            '''
            for i in range(len(copy_vehicle_routes)):
                if not copy_vehicle_routes[i]:
                    for key in vehicles[i].is_blocked.keys():
                        if not vehicles[i].is_blocked[key] and not key.is_saboteur:
                            tn += 1
                        if not vehicles[i].is_blocked[key] and key.is_saboteur:
                            fn += 1
                        if vehicles[i].is_blocked[key] and key.is_saboteur:
                            tp += 1
                        if vehicles[i].is_blocked[key] and not key.is_saboteur:
                            fp += 1
                    total_finished += 1
            vehicles = [copy_vehicles[i] for i in range(len(copy_vehicles)) if copy_vehicle_routes[i]]
            vehicle_routes = [copy_vehicle_route for copy_vehicle_route in copy_vehicle_routes if copy_vehicle_route]

            del copy_vehicle_routes
            del copy_vehicles

        # if iteration != iterations_in_experiment - 1:
        if total_generated < total_UV:
            if total_UV - total_generated >= 4:
                new_vehicle_amount = np.random.choice(np.array([0, 1, 2, 3, 4]), p=[0.2, 0.2, 0.2, 0.2, 0.2])
            elif total_UV - total_generated == 3:
                new_vehicle_amount = np.random.choice(np.array([0, 1, 2, 3]), p=[0.25, 0.25, 0.25, 0.25])
            elif total_UV - total_generated == 2:
                new_vehicle_amount = np.random.choice(np.array([0, 1, 2]), p=[0.33, 0.33, 0.34])
            elif total_UV - total_generated == 1:
                new_vehicle_amount = np.random.choice(np.array([0, 1]), p=[0.5, 0.5])
            else:
                new_vehicle_amount = 0
            total_generated += new_vehicle_amount
            for new_vehicle in range(new_vehicle_amount):
                if new_vehicle == 0 and total_saboteur_amount <= total_vehicle_amount * saboteur_percentage:
                    is_saboteur = True
                else:
                    is_saboteur = False
                vehicle = Vehicle(
                    initial_speed=1,
                    field=field,
                    existing_vehicle_routes=vehicle_routes,
                    is_saboteur=is_saboteur
                )
                vehicle_route = vehicle.determine_vehicle_route()
                if vehicle_route:
                    vehicles.append(vehicle)
                    vehicle_routes.append(vehicle_route)
                    total_vehicle_amount += 1
                    if is_saboteur:
                        total_saboteur_amount += 1

            # Проверка на наличие конфликтов
            conflicts_are_presented, conflicts_info = check_if_conflicts_are_presented(vehicle_routes, iteration)
            conflicts_info_check = []
            while conflicts_are_presented:
                for conflict_info in conflicts_info:
                    if conflicts_info_check:
                        if conflict_info not in conflicts_info_check:
                            break
                    vehicle_routes, deleted_cells, difference, saboteur = \
                        resolve_vehicle_conflicts(vehicles, vehicle_routes, conflict_info)

                    if deleted_cells:
                        total_step_amount -= difference
                        total_generated -= 1
                        if saboteur:
                            total_saboteur_amount -= 1
                        # total_vehicle_amount -= 1  # RETURN
                    conflicts_are_presented, conflicts_info_check = check_if_conflicts_are_presented(vehicle_routes,
                                                                                                     iteration)
                conflicts_are_presented, conflicts_info = check_if_conflicts_are_presented(vehicle_routes, iteration)

            for vehicle in vehicles:
                for new_vehicle in vehicles:
                    if vehicle != new_vehicle:
                        vehicle.add_vehicle(new_vehicle, iteration)

    for vehicle_route in vehicle_routes:
        total_step_amount += len(vehicle_route)
    with open(path_to_file, "a") as file:
        file.write(str(tp) + '\t' + str(fp) + '\t' + str(tn) + '\t' + str(fn) + '\n')

    # print(truth_total / total_not_full_data_situation, total_not_full_data_situation)
    return total_step_amount / total_vehicle_amount


def main():
    step_amount_per_vehicle = 0
    for mode in ["Classic reputation", "Game theory"]:
        for percent in range(1): # range(1, 5, 1):
            global experiment_type, saboteur_percentage, path_to_file
            experiment_type = mode
            saboteur_percentage = percent/10
            path_to_file = mode + " " + str(saboteur_percentage) + ".txt"
            open(path_to_file, "w")
            for experiment in range(experiment_amount):
                step_amount_per_vehicle += start_moving(experiment)




    '''
    open(path_to_file, "w")
    # with open(path_to_file, "w") as file:
        # file.write(experiment_type + ", " + str(saboteur_percentage) + " saboteurs\ntp  fp  tn  fn\n")

    for experiment in range(experiment_amount):
        step_amount_per_vehicle += start_moving(experiment)
    '''

if __name__ == "__main__":
    main()
